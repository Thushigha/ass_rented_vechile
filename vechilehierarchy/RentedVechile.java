package ap.vechilehierarchy;

public class RentedVechile {
	private double basefee;

	public RentedVechile(double basefees) {
		super();
		this.basefee = basefees;
	}

	public double getBasefee() {
		return basefee;
	}

	public void setBasefee(double basefee) {
		this.basefee = basefee;
	}
}
