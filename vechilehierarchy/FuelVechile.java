package ap.vechilehierarchy;

public class FuelVechile extends RentedVechile {
	private double nbKms;

	public FuelVechile(double basefee, double nbKms) {
		super(basefee);
		this.nbKms = nbKms;
	}

	public double getMileageFees() {
		if (nbKms < 100) {
			return 0.2 * nbKms;
		}

		else if (nbKms <= 400 || nbKms >= 100) {
			return 0.3 * nbKms;
		} else {
			return 0.3 * 400 + (nbKms - 400) * 0.5;
		}

	}
}
