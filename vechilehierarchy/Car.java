package ap.vechilehierarchy;

public class Car extends FuelVechile {
	private int nbSeats;

	public Car(double basefee, double nbKms, double nbSeats) {
		super(basefee, nbKms);
		this.nbSeats=(int)nbSeats;
	}

	public int getNbSeats() {
		return nbSeats;
	}

	public void setNbSeats(int nbSeats) {
		this.nbSeats = nbSeats;
	}

	@Override
	public double getMileageFees() {
		return super.getMileageFees();
	}

	public double getCost() {
		return (nbSeats * getBasefee()) + getMileageFees();
	}

}
