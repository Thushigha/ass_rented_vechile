package ap.vechilehierarchy;

import java.util.Scanner;

public class TestDemo {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Welcome to Vechile Rental Company");

		String[] rentedVechile = { "1.Car", "2.Truck", "3.Bicycle" };

		for (int i = 0; i < rentedVechile.length; i++) {
			System.out.println(rentedVechile[i]);
		}
		System.out.println("Please enter the the number");
		int choice = input.nextInt();
		switch (choice) {
		case 1:
			Car car = new Car(200, 150, choice);
			car.setNbSeats(10);
			System.out.println("You are selecting the Suzuki car");
			System.out.println("Number of seats: " + car.getNbSeats());
			System.out.println("Cost of rent: " + "Rs." + car.getCost());
		case 2:
			Truck truck = new Truck(500, 30);
			System.out.println("You are selecting the Majo Truck ");
			truck.setCapacity(5000);
			System.out.println("Capacity of Truck: " + truck.getCapacity());
			System.out.println("Cost of rent: " + "Rs." + truck.getCost());
		case 3:
			Bicycle bicycle = new Bicycle(100, 30);
			System.out.println("You are selecting the Race bicycle");
			System.out.println("Number of days for rent: " + bicycle.getNbDays());
			System.out.println("Cost of rent: " + "Rs." + bicycle.getCost());
		}

	}

}
